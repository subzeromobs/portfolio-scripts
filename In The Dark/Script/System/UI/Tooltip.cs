﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode()]
public class Tooltip : MonoBehaviour
{
    public TextMeshProUGUI ItemName;
    public TextMeshProUGUI ItemAmount;
    public TextMeshProUGUI ItemDes;
    public LayoutElement layoutElement;
    public int CharterWrapLimit; 
    public Transform ui;

    public RectTransform rectTransform;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    private void update()
    {
        if(Application.isEditor)
        {
            int ItemNameLength = ItemName.text.Length;
            int ItemAmountLength = ItemAmount.text.Length;
            int ItemDesLength = ItemDes.text.Length;

            layoutElement.enabled = (ItemNameLength > CharterWrapLimit || ItemDesLength > CharterWrapLimit || ItemAmountLength > CharterWrapLimit) ? true : false;
        }
        

        Vector2 position = Input.mousePosition;

        float pivotX = position.x / Screen.width;
        float pivotY = position.y / Screen.height;

        rectTransform.pivot = new Vector2(pivotX , pivotY);
        transform.position = Input.mousePosition;

        Debug.Log("WeaponNum = " + Input.mousePosition);
        Debug.Log("WeaponNum2 = " + position);
        Debug.Log("test = " + transform.position);
    }

}
