using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiInteract : MonoBehaviour
{
    [SerializeField]
    private InventoryUi Ivn;

    private bool IvnOpn = false;
    // Start is called before the first frame update
    void Start()
    {
        // Ivn.IvnUiOff();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown("i"))
        {
            if(!IvnOpn)
            {
                // Ivn.IvnUiOn();
                IvnOpn = true;
                Cursor.lockState = CursorLockMode.Confined;
            }
            else
            {
                // Ivn.IvnUiOff();
                IvnOpn = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }
}
