using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public Transform itemParent;
    public Button dropBtn;
    public Image icon;
    DefaultObj item;
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void AddItem(DefaultObj newItem)
    {
        item = newItem;
        icon.sprite = item.ItemIcon;
        icon.enabled = true;
    }

    public void ClearSlot()
    {
        item = null;
        icon.sprite = null;
        icon.enabled = false;
    }

    public void ShowUseUi()
    {
        // show the using ui tab when click
        // and if one ui tab is on and when user click the other one disable the previous one
    }
}
