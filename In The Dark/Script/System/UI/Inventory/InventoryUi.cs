using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class InventoryUi : MonoBehaviour
{
    public GameObject IvnUi;
    public InventoryObj Ivn;

    public Transform ivnSlotParent;
    InventorySlot[] ivnSlots;
    void Start()
    {
        ivnSlots = ivnSlotParent.GetComponentsInChildren<InventorySlot>();
    }

    void Update()
    {
        
    }
    void IvnUpdateUi()
    {
        for(int i = 0;i < ivnSlots.Length; i++ )
        {
            if(i < Ivn.itemList.Count)
            {
                ivnSlots[i].AddItem(Ivn.itemList[i]);
            }
            else
            {
                ivnSlots[i].ClearSlot();
            }
        }
    }
}
