using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Inventory", menuName ="Inventory")]
public class InventoryObj : ScriptableObject
{
    // ivn space
    public int space = 20;
    // ivn list
    public List<DefaultObj> itemList = new List<DefaultObj>();
    // adding item to ivn
    public void Add(DefaultObj item)
    {
        if(!item.InteractType)
        {
            if(itemList.Count >= space) 
            {
                Debug.Log("ivn full");
            }
            else if(space <= 0)
            {
                Debug.Log("No Ivn space");
            }
            itemList.Add(item);
        }
    }
    // removing item from ivn
    public void Remove(DefaultObj item)
    {
        itemList.Remove(item);
    }
}
