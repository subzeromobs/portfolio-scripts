﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Journal_System : MonoBehaviour
{
    public bool note1 = false;
    public bool note2 = false;
    public bool note3 = false;
    public bool note4 = false;
    public bool note5 = false;
    public bool note6 = false;
    public bool HaveNote = false;
    public bool ViewJournal = false;
    public bool InMenu = true;
    public AudioSource PlayerAudioSource;
    public AudioClip OpenNote;
    public AudioClip CloseNote;
    public GameObject NoteBook;
    public GameObject JournalTittle_1;
    public GameObject JournalTittle_2;
    public GameObject JournalTittle_3;
    public GameObject JournalTittle_4;
    public GameObject JournalTittle_5;
    public GameObject JournalTittle_6;

    public GameObject JournalTittle_Ui;
    public GameObject JournalTittle_Ui_Text;
    public GameObject JournalTittle_Ui_Exit;
    // public gameobject ui for tittle / button that can run on click event

    // Start is called before the first frame update
    void Start()
    {
        NoteBook.SetActive(false);
        JournalTittle_Ui.SetActive(false);
        JournalTittle_Ui_Text.SetActive(false);
        JournalTittle_Ui_Exit.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(!InMenu)
        {
          if(Input.GetKeyDown("j"))
            {
                if(ViewJournal == false)
                {
                    ViewJournal = true;
                    Cursor.lockState = CursorLockMode.Confined;
                    GameObject.Find("Item_Manager").GetComponent<Item_Manager>().looknote = true;
                    Time.timeScale = 0;
                    NoteBook.SetActive(true);
                    PlayerAudioSource.PlayOneShot(OpenNote);
                    JournalTittle_Ui.SetActive(true);

                    if(note1)
                    {
                        JournalTittle_1.SetActive(true);
                    }
                    
                    if(note2)
                    {
                        JournalTittle_2.SetActive(true);
                    }
                    
                    if(note3)
                    {
                        JournalTittle_3.SetActive(true);
                    }
                    
                    if(note4)
                    {
                        JournalTittle_4.SetActive(true);
                    }
                    
                    if(note5)
                    {
                        JournalTittle_5.SetActive(true);
                    }
                    
                    if(note6)
                    {
                        JournalTittle_6.SetActive(true);
                    }
                }
                else
                {
                    ViewJournal = false;
                    Cursor.lockState = CursorLockMode.Locked;
                    NoteBook.SetActive(false);
                    GameObject.Find("Item_Manager").GetComponent<Item_Manager>().looknote = false;
                    // GameObject.Find("Note_1").GetComponent<Note_1>().UI.SetActive(false);
                    JournalTittle_Ui.SetActive(false);
                    JournalTittle_Ui_Text.SetActive(false);
                    JournalTittle_Ui_Exit.SetActive(false);
                    Time.timeScale = 1;
                    PlayerAudioSource.PlayOneShot(CloseNote);
                    JournalTittle_1.SetActive(false);
                    JournalTittle_2.SetActive(false);
                    JournalTittle_3.SetActive(false);
                    JournalTittle_4.SetActive(false);
                    JournalTittle_5.SetActive(false);
                    JournalTittle_6.SetActive(false);
                }
                
            }  
        }
        

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            NoteBook.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            JournalTittle_1.SetActive(false);
            JournalTittle_2.SetActive(false);
            JournalTittle_3.SetActive(false);
            JournalTittle_4.SetActive(false);
            JournalTittle_5.SetActive(false);
            JournalTittle_6.SetActive(false);
            Time.timeScale = 1;
            // GameObject.Find("Note_1").GetComponent<Note_1>().UI.SetActive(false);
            JournalTittle_Ui.SetActive(false);
            JournalTittle_Ui_Text.SetActive(false);
            JournalTittle_Ui_Exit.SetActive(false);
            if(ViewJournal == true)
            {
                ViewJournal = false;
                PlayerAudioSource.PlayOneShot(CloseNote);
            }
        }
    }

}
