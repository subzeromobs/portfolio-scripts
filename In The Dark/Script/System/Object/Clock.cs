﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clock : MonoBehaviour
{
    public float Second = 60;

    public float Minute = 60;

    public float Hour = 0;

    public float Day = 1;

    public bool DayTime = false;
    
    public bool NightTime = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Second -= 1 * Time.deltaTime;

        if(Second <= 0)
        {
            Minute -= 1;
            Second = 60;
        }

        if(Minute <= 0)
        {
            Hour -= 1;
            Minute = 60;
        }

        if(Hour >= 24)
        {
            Hour = 0;
            Day += 1;
        }

        if(Hour >= 6)
        {
            DayTime = true;
            NightTime = false;
        }

        if(Hour >= 18)
        {
            NightTime = true;
            DayTime = false;
        }
    }
}
