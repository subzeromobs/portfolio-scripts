﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Item : MonoBehaviour
{
    public DefaultObj ScriptObject;

    public ItemMaster ItemMaster;
    public AudioSource PlayerAudioSource;
    public TextMeshProUGUI Diolouge;
    public bool InPickUpArea = false;
    public bool IntType = false;
    public bool ItmType = false;
    public GameObject ItemModel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if(ScriptObject.InteractType == true)
       {
           IntType = true;
       }

       if(ScriptObject.ItemType == true)
       {
           ItmType = true;
       }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            InPickUpArea = true;
            Debug.Log("can Be Picked up");
        }
    }

    public void DisplayName()
    {
        if(IntType == true)
        {
            Diolouge.text = "Press E to use" + ScriptObject.ObjectName ;
        }

        // if(IntType == true && ScriptObject.Bed == true)
        // {
        //     Diolouge.text = "Press E to sleep";
        // }

        if(ItmType == true)
        {
            Diolouge.text = "Press E to keep" + ScriptObject.ObjectName ;
        }
    }

    public void Interaction()
    {
        if(IntType == true)
        {
            Diolouge.text = "Press E to use" + ScriptObject.ObjectName ;
        }
    }

    public void DestroyThisObj()
    {
        Destroy(ItemModel);
    }
}
