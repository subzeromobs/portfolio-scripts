﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    public Player_Stat Player_Stat;
    public Object ScriptObject;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Eat()
    {
        Player_Stat.Hunger += ScriptObject.Hunger;
    }
}
