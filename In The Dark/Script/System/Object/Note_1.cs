using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Note_1 : MonoBehaviour
{
    public Story ScriptObject;
    public TextMeshProUGUI NoteTitle;
    public GameObject NoteTitleButton;
    public GameObject NoteCloseButton;
    public TextMeshProUGUI NoteName;
    public TextMeshProUGUI Story;
    public TextMeshProUGUI Diolouge;
    public GameObject UI;
    public GameObject NoteBook;
    public AudioSource PlayerAudioSource;
    public AudioClip Note_Open;
    public AudioClip Note_Close;
    public bool NoteCollected = false;
    

    void Start()
    {
        UI.SetActive(false);
    }

    public void ShowNote()
    {
        NoteTitle.text = ScriptObject.StoryName;
        NoteName.text = ScriptObject.StoryName;
        Story.text = ScriptObject.NoteStory;
        UI.SetActive(true);
        NoteTitleButton.SetActive(false);
        NoteCloseButton.SetActive(true);
        PlayerAudioSource.PlayOneShot(Note_Open);
    }

    public void NoNote()
    {
        UI.SetActive(false);
        NoteTitleButton.SetActive(true);
        NoteCloseButton.SetActive(false);
        PlayerAudioSource.PlayOneShot(Note_Close);
    }

    public void PickNote()
    {
        if(ScriptObject.Series == 1)
        {
            GameObject.Find("Player_Body").GetComponent<Journal_System>().note1 = true;
            NoteTitle.text = ScriptObject.StoryName;
            NoteBook.SetActive(false);
        }
        
        if(ScriptObject.Series == 2)
        {
            GameObject.Find("Player_Body").GetComponent<Journal_System>().note2 = true;
            NoteTitle.text = ScriptObject.StoryName;
            NoteBook.SetActive(false);
        }

        if(ScriptObject.Series == 3)
        {
            GameObject.Find("Player_Body").GetComponent<Journal_System>().note3 = true;
            NoteTitle.text = ScriptObject.StoryName;
            NoteBook.SetActive(false);
        }

        if(ScriptObject.Series == 4)
        {
            GameObject.Find("Player_Body").GetComponent<Journal_System>().note4 = true;
            NoteTitle.text = ScriptObject.StoryName;
            NoteBook.SetActive(false);
        }

        if(ScriptObject.Series == 5)
        {
            GameObject.Find("Player_Body").GetComponent<Journal_System>().note5 = true;
            NoteTitle.text = ScriptObject.StoryName;
            NoteBook.SetActive(false);
        }

        if(ScriptObject.Series == 6)
        {
            GameObject.Find("Player_Body").GetComponent<Journal_System>().note6 = true;
            NoteTitle.text = ScriptObject.StoryName;
            NoteBook.SetActive(false);
        }
    }

    void update()
    {
        
    }
}
