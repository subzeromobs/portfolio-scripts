using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponInteract : MonoBehaviour
{
    public Weapon EquipedWeapon;

    private InventoryObj Bag;

    public Item Testitem;

    public Enemy Enemy;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown("r"))
        {
            EquipedWeapon.EjectMag();
            // Bag.AddItem(Testitem.ScriptObject, 1);
        }

        if(EquipedWeapon.Reloading == true)
        {
            if(Input.GetKeyDown("r"))
            {
                if(EquipedWeapon.MagType == true)
                {
                    EquipedWeapon.InsertMag();
                }
                if(EquipedWeapon.ShellType == true)
                {
                    EquipedWeapon.InsertShell();
                }
            }
        }

        if(Input.GetKeyDown("c"))
        {
            EquipedWeapon.CheckMag();
        }

        if(Input.GetButtonDown("mouse1"))
        {
            if(EquipedWeapon.MagType == true)
            {
                EquipedWeapon.Shooting();
            }

            if(EquipedWeapon.ShellType == true)
            {
                EquipedWeapon.Shooting();
            }
        }
    }
}
