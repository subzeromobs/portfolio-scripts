using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Weapon : MonoBehaviour
{
    public int Overheat = 0;

    public int WeaponDamage;

    public bool GunIsEquip = false;

    public bool GunJam = false;
     
    public bool Reloading = false;

    public bool  NotChamber  = false;

    public bool ShellType = false;

    public bool MagType  = false;

    private InventoryObj Bag;

    public WeaponType Firearm;

    public Weapon weapon;

    public MagazineType Mag;

    public Player_Stat Player_Stat;

    public AudioSource GunAudioSource;

    void Start()
    {
        if(Firearm.Shell == true)
        {
            ShellType = true;
        }

        if(Firearm.Magazine == true)
        {
            MagType = true;
        }

        WeaponDamage = Firearm.Damage;
    }

    // Update is called once per frame
    void Update()
    {
        if(Overheat >= 30)
        {
            // guns recoil get worse
        }

        if((ShellType == true) && (Firearm.Rounds <= Firearm.AmmoPerMag))
        {
            Reloading =  true;
            // NotChamber = true;
        }
        else
        {
            Reloading =  false;
        }
        
        // press R again to reload mag when reloading a shell weopon player can press t to load the gun with that amount of ammo reloading and shoot

        // chance of gun getting jam and increase when condition is lower then a certain amount

        // Shooting and increase in shooting / moving would increase recoil

        // right click on this gun and clicking mantain will help mantain the weapon 
    }

    public void EjectMag()
    {
        if(Firearm.Magazine == true)
        {
            Firearm.Rounds = 0;
            // Play EjectMag animation
            Reloading =  true;
            // Bag.AddItem(Mag, 1);
            
        }
    }

    public void Shooting()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit; 

        if (Physics.Raycast(ray, out hit))
        {
            var Enemy = hit.transform;

            if(Enemy.CompareTag("Enemy"))
            {
                Enemy.GetComponent<Enemy>().Hp -= WeaponDamage;
            }
        }
        Firearm.Rounds -= 1;
        Overheat += Firearm.OverheatPershot;
    }

    public void InsertMag()
    {
        if(Firearm.Magazine == true)
        {
            Firearm.Rounds = Firearm.AmmoPerMag;
            // Play InsertMag animation
            Reloading =  false;
        }
    }

    public void InsertShell()
    {
        Firearm.Rounds += 1;
        // Play InsertMag animation
    }

    public void ChamberShell()
    {
        Firearm.Rounds += 1;
        // NotChamber = false;
    }

    public void CheckMag()
    {
        // play check mag animation
        // Diolouge.text = Firearm.Rounds;
    }
}
