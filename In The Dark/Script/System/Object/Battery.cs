﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Battery : MonoBehaviour
{
    public Object item;
    public TextMeshProUGUI ObjName;
    public TextMeshProUGUI ObjDescription;
    public TextMeshProUGUI InvDes;
    public GameObject UI;

    void Start()
    {
        ObjName.text = "";
        ObjDescription.text = "";
        InvDes.text = "";
    }

    void update()
    {
        
    }

    void OnMouseOver()
    {
        ObjName.text = item.ObjectName;
        ObjDescription.text = item.description;
        InvDes.text = item.InvText;
    }

    void OnMouseExit()
    {
        ObjName.text = "";
        ObjDescription.text = "";
        InvDes.text = "";
    }

    void OnMouseDown() 
    {
        GameObject.Find("Player_Body").GetComponent<Player_Movement>().FlashBattery += 10.0f;
        GameObject.Find("Player").GetComponent<Inventory>().BatteryNum -= 1f;
    }

}
