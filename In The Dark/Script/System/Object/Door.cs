﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Door : MonoBehaviour
{
    public Animator transition;
    public Door_Type DoorObject;
    public float transitionTime = 3f;
    
    public Transform Player;
    public Transform SpawnBack;
    public Transform Door_In_Trans;
    public Transform Door_Out_Trans;

    public GameObject Door_In_Tri;
    public GameObject Door_Out_Tri;

    public AudioSource PlayerAudioSource;

    public AudioClip Door_Open;

    public AudioClip Door_Lock;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter()
    {
        if(DoorObject.DoorNum == 1)
        {
            if(DoorObject.Door_In == true)
            {
                GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door1_In = true;
            }
            else
            {
                GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door1_Out = true;
            }
        }
        else if(DoorObject.DoorNum == 2)
        {
            if(DoorObject.Door_In == true)
            {
                GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door2_In = true;
            }
            else
            {
                GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door2_Out = true;
            }
        }
        else if(DoorObject.DoorNum == 3)
        {
            if(DoorObject.Door_In == true)
            {
                GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door3_In = true;
            }
            else
            {
                GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door3_Out = true;
            }
        }
        else if(DoorObject.DoorNum == 4)
        {
            if(DoorObject.Door_In == true)
            {
                GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door4_In = true;
            }
            else
            {
                GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door4_Out = true;
            }
        }
        else if(DoorObject.DoorNum == 5)
        {
            if(DoorObject.Door_In == true)
            {
                GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door5_In = true;
            }
            else
            {
                GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door5_Out = true;
            }
        }
        else if(DoorObject.DoorNum == 7)
        {
            if(DoorObject.Door_In == true)
            {
                if(DoorObject.IsLock == true)
                {
                    GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door1_In_Lock = true;
                }
                else
                {
                    GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door1_In_Lock = true;
                }
            }
            else
            {
                if(DoorObject.IsLock == true)
                {
                    GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door1_Out_Lock = true;
                }
                else
                {
                    GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door1_Out_Lock = true;
                }
            }
        }
        else if(DoorObject.DoorNum == 8)
        {
            if(DoorObject.Door_In == true)
            {
                if(DoorObject.IsLock == true)
                {
                    GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door_Final_In_Lock = true;
                }
                else
                {
                    // GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door_Final_In_Lock = true;
                }
            }
            else
            {
                if(DoorObject.IsLock == true)
                {
                    GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door_Final_Out_Lock = true;
                }
                else
                {
                    // GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door_Final_Out_Lock = true;
                }
            }
        }
    }

    void OnTriggerExit()
    {
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door1_In = false;
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door1_Out = false;
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door2_In = false;
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door2_Out = false;
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door3_In = false;
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door3_Out = false;
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door4_In = false;
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door4_Out = false;
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door5_In = false;
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door5_Out = false;
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door1_In_Lock = false;
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door1_Out_Lock = false;
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door_Final_In_Lock = false;
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Door_Final_Out_Lock = false;
    }

    public void DoorIn()
    {
        StartCoroutine(DoorInAnim());
    }

    public void DoorOut()
    {
        StartCoroutine(DoorOutAnim());
    }

    public void DoorLock()
    {
        PlayerAudioSource.PlayOneShot(Door_Lock);
    }

    public void Key_Take_Spawn()
    {
        Player.position = SpawnBack.position;
    }

    IEnumerator DoorInAnim()
    {
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Opening_Door = true;
        Door_Out_Tri.SetActive(false);
        GameObject.Find("Player_Body").GetComponent<Player_Movement>().PlayerMovePause();
        GameObject.Find("Player_Cam").GetComponent<Mouse_Look>().PlayerMousePause();
        PlayerAudioSource.PlayOneShot(Door_Open);
        transition.SetBool("Fade",true);
        yield return new WaitForSeconds(transitionTime);
        Player.position = Door_Out_Trans.position;
        transition.SetBool("Fade",false);
        GameObject.Find("Player_Body").GetComponent<Player_Movement>().PlayerMoveUnpause();
        GameObject.Find("Player_Cam").GetComponent<Mouse_Look>().PlayerMouseUnpause();
        Door_Out_Tri.SetActive(true);
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Opening_Door = false;
    }

    IEnumerator DoorOutAnim()
    {
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Opening_Door = true;
        Door_In_Tri.SetActive(false);
        GameObject.Find("Player_Body").GetComponent<Player_Movement>().PlayerMovePause();
        GameObject.Find("Player_Cam").GetComponent<Mouse_Look>().PlayerMousePause();
        PlayerAudioSource.PlayOneShot(Door_Open);
        transition.SetBool("Fade",true);
        yield return new WaitForSeconds(transitionTime);
        Player.position = Door_In_Trans.position;
        transition.SetBool("Fade",false);
        GameObject.Find("Player_Body").GetComponent<Player_Movement>().PlayerMoveUnpause();
        GameObject.Find("Player_Cam").GetComponent<Mouse_Look>().PlayerMouseUnpause();
        Door_In_Tri.SetActive(true);
        GameObject.Find("Item_Manager").GetComponent<Item_Manager>().Opening_Door = false;
    }
}
