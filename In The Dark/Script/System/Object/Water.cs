﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
    public Player_Stat Player_Stat;
    public Object ScriptObject;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Drink()
    {
        Player_Stat.Thirst += ScriptObject.Thirst;
    }
}
