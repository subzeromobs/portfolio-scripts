using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Note_2 : MonoBehaviour
{
    public Story item;
    public TextMeshProUGUI NoteName;
    public TextMeshProUGUI Story;
    public GameObject UI;

    void Start()
    {
        Debug.Log("note number" + item.Series);
        if(item.Series == 1)
        {
            Debug.Log("note1");
        }
        else if(item.Series == 2)
        {
            Debug.Log("note2");
        }
    }

    public void ShowNote()
    {
        NoteName.text = item.StoryName;
        Story.text = item.NoteStory;
        Debug.Log(NoteName.text);
        Debug.Log(Story.text);
    }

    void update()
    {

    }

    void OnMouseOver()
    {
       
    }

    void OnMouseExit()
    {
        
    }

    void OnMouseDown() 
    {
        
    }

}
