﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event_Trigger : MonoBehaviour
{
    public Event_Type Event;
    public Animator Animator;
    public GameObject Ghost;
    public GameObject BackGhost;
    public GameObject ItemSpawn;
    public GameObject FinalState;
    public GameObject PlayState;
    public Transform PlayerTrans;
    public Transform SpawnBack;
    public float FreezeDuration = 1f;
    private bool TakenKey = false;
    private bool Triggered = false;
    public AudioSource PlayerAudioSource;
    public AudioSource ScareAudioSource1;
    public AudioClip Scare_Clip1;
    public AudioClip Scare_Clip2;
    void start()
    {
        ItemSpawn.SetActive(false);
        TakenKey = false;
        Triggered = false;
    }

    void update()
    {
        if(GameObject.Find("Player_Body").GetComponent<Player_Movement>().PlayerHasKey1 == true)
        {
            TakenKey = true;
            PlayerAudioSource.PlayOneShot(Scare_Clip2);
        }
    }
    void OnTriggerEnter()
    {
        if(Event.EventNum == 1)
        {
            StartCoroutine(GhostMove1());
        }
        if(Event.AnimScare == true)
        {
            Animator.SetBool("Is_Trigger",true);
        }
        if(Event.SoundScare == true && Triggered == false)
        {
            ScareAudioSource1.PlayOneShot(Scare_Clip1);
            Triggered = true;
        }
        if(Event.PlayerFreze == true)
        {
            StartCoroutine(PlayerFreeze());
        }
        if(Event.ItemActive == true)
        {
            ItemSpawn.SetActive(true);
        }
        if(Event.KeyTake == true)
        {   
            // if(TakenKey == true)
            // {
            //     StartCoroutine(PlayerFreezeSpawnBack());
            // }
        }

        if(GameObject.Find("Player_Body").GetComponent<Player_Movement>().PlayerHasKey1 == true)
        {
            Debug.Log("key is taken");
            FinalState.SetActive(true);
            PlayState.SetActive(false);
            PlayerTrans.position = SpawnBack.position;
        }
    }

    IEnumerator GhostMove1()
    {
        BackGhost.SetActive(true);
        yield return new WaitForSeconds(3f);
        BackGhost.SetActive(false);
        Triggered = true;
    }

    IEnumerator PlayerFreeze()
    {
        GameObject.Find("Player_Body").GetComponent<Player_Movement>().PlayerMovePause();
        GameObject.Find("Player_Cam").GetComponent<Mouse_Look>().PlayerMousePause();
        yield return new WaitForSeconds(FreezeDuration);
        GameObject.Find("Player_Body").GetComponent<Player_Movement>().PlayerMoveUnpause();
        GameObject.Find("Player_Cam").GetComponent<Mouse_Look>().PlayerMouseUnpause();
        Triggered = true;
    }

    // IEnumerator PlayerFreezeSpawnBack()
    // {
    //     GameObject.Find("Player_Body").GetComponent<Player_Movement>().PlayerMovePause();
    //     GameObject.Find("Player_Cam").GetComponent<Mouse_Look>().PlayerMousePause();
    //     PlayerTrans.position = SpawnBack.position;
    //     yield return new WaitForSeconds(FreezeDuration);
    //     GameObject.Find("Player_Body").GetComponent<Player_Movement>().PlayerMoveUnpause();
    //     GameObject.Find("Player_Cam").GetComponent<Mouse_Look>().PlayerMouseUnpause();
    //     Triggered = true;
    // }
}
