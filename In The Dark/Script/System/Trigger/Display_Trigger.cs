﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Display_Trigger : MonoBehaviour , IPointerEnterHandler, IPointerExitHandler
{
    public void OnPointerEnter(PointerEventData eventData)
    {
        Display_System.Show();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Display_System.Hide();
    }

    public void OnMouseEnter()
    {
        Display_System.Show();
    }

    public void OnMouseExit()
    {
        Display_System.Hide();
    }
}
