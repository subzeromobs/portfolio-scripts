using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Light_Trigger : MonoBehaviour
{
    public GameObject Light1;
    public GameObject Light2;
    public GameObject Light3;
    public float Light_Stop_Time = 1.5f;

    private bool Is_Trigger = false;

    void start()
    {

    }

    void update()
    {

    }
    void OnTriggerEnter()
    {
        if(Is_Trigger == false)
        {
            StartCoroutine(Light_Flicker());
        }
        
    }
    IEnumerator Light_Flicker()
    {
        Is_Trigger = true;
        Light1.SetActive(false);
        Light2.SetActive(false);
        Light3.SetActive(false);
        yield return new WaitForSeconds(Light_Stop_Time);
        Light1.SetActive(true);
        Light2.SetActive(true);
        Light3.SetActive(true);
    }
}
