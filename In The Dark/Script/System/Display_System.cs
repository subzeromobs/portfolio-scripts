﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Display_System : MonoBehaviour
{
    private static Display_System current;

    public Tooltip tooltip;

    public void awake()
    {
        current = this;
    }

    public static void Show()
    {
        current.tooltip.gameObject.SetActive(true);
    }

    public static void Hide()
    {
        current.tooltip.gameObject.SetActive(false);
    }
}
