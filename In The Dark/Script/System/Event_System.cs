using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event_System : MonoBehaviour
{
    public Animator transition;
    public Animator Animator1;
    public Animator Animator2;
    public Animator Animator3;
    public AudioSource PlayerAudioSource;
    public AudioSource RadioAudioSource1;
    public AudioSource RadioAudioSource2;
    public AudioClip Radio_Clip1;
    public AudioClip Radio_Clip2;
    public GameObject FinalState;
    public GameObject StartState;
    public bool RadioPlayed_1 = false;
    public bool RadioPlayed_2 = false;
    public bool Final = false;

    void start()
    {
        FinalState.SetActive(false);
    }

    void update()
    {
        // if(Final == true)
        // {
        //     FinalState.SetActive(true);
        // }
        
        // if(GameObject.Find("Player_Body").GetComponent<Journal_System>().note6 == true)
        // {
            
        // }

        // if(GameObject.Find("Player_Body").GetComponent<Journal_System>().note3 == true)
        // {
            
        // }

        // if(GameObject.Find("Player_Body").GetComponent<Journal_System>().note1 == true)
        // {
        //     StartState.SetActive(false);
        // }
    }

    public void Note3Taken()
    {
        if(RadioPlayed_2 == false)
        {
            Debug.Log("Radio2 is played");
            RadioAudioSource2.PlayOneShot(Radio_Clip2);
            RadioPlayed_2 = true;
        }
    }
    public void Note6Taken()
    {
        if(RadioPlayed_1 == false)
        {
            Debug.Log("Radio1 is played");
            RadioAudioSource1.PlayOneShot(Radio_Clip1);
            RadioPlayed_1 = true;
        }
    }
    public void LastState()
    {
        FinalState.SetActive(true);
        StartState.SetActive(false);
    }
    public void DeathState()
    {
        StartCoroutine(DeathStateAnim());
    }
    IEnumerator DeathStateAnim()
    {
        Animator1.SetBool("Trigger",true);
        Animator2.SetBool("Trigger",true);
        Animator3.SetBool("Trigger",true);
        yield return new WaitForSeconds(9f);
        transition.SetBool("Fade",true);
        yield return new WaitForSeconds(1f);
        Application.Quit();
    }
}
