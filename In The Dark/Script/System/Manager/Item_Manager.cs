﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Item_Manager : MonoBehaviour
{
    public TextMeshProUGUI text;
    public bool looknote = false;
    public GameObject Blur;
    public GameObject note1;
    public GameObject note2;
    public GameObject Battery;
    public GameObject Key1;
    public GameObject flashlight;
    public GameObject antidote;
    public AudioSource PlayerAudioSource;
    public AudioClip Bat_Take;
    public AudioClip Key_Take;
    public AudioClip Flash_Take;
    public AudioClip Door_Open;
    public AudioClip Note_Take;
    public Transform Ivn1;
    public Transform Ivn2;

    public Transform Player;

    public bool Door1_In = false;
    public bool Door1_Out = false;
    public bool Door2_In = false;
    public bool Door2_Out = false;
    public bool Door3_In = false;
    public bool Door3_Out = false;
    public bool Door4_In = false;
    public bool Door4_Out = false;
    public bool Door5_In = false;
    public bool Door5_Out = false;
    public bool Door1_In_Lock = false;
    public bool Door1_Out_Lock = false;
    public bool Door_Final_In_Lock = false;
    public bool Door_Final_Out_Lock = false;

    public bool Opening_Door = false;
    public Transform Door1_In_Trans;
    public Transform Door1_Out_Trans;

    public 

    // Start is called before the first frame update
    void Start()
    {
        Blur.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit; 
        Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;
        Debug.DrawRay(transform.position, forward, Color.green);

        if (Physics.Raycast(ray, out hit, 1))
        {   
            var Item = hit.transform;

            if(Item.CompareTag("Battery"))
            {
                text.text = "Press E to pick up Battery";
                if(Input.GetKeyDown("e"))
                {
                    PlayerAudioSource.PlayOneShot(Bat_Take);
                    GameObject.Find("Player").GetComponent<Inventory>().BatteryNum += 1f;
                    Destroy(Battery);
                }
            }
            else if(Item.CompareTag("Key"))
            {
                text.text = "Press E to pick up key";
                if(Input.GetKeyDown("e"))
                {
                    GameObject.Find("Player_Body").GetComponent<Player_Movement>().PlayerHasKey1 = true;
                    GameObject.Find("Scare_Trigger").GetComponent<Event_System>().Final = true;
                    GameObject.Find("Scare_Trigger").GetComponent<Event_System>().LastState();
                    PlayerAudioSource.PlayOneShot(Key_Take);
                    GameObject.Find("Key_1").GetComponent<Door>().Key_Take_Spawn();
                    Key1.SetActive(false);
                }
            }
            else if(Item.CompareTag("Flashlight"))
            {
                text.text = "Press E to pick up flashlight";
                if(Input.GetKeyDown("e"))
                {
                    GameObject.Find("Player_Body").GetComponent<Player_Movement>().haveflashlight = true;
                    PlayerAudioSource.PlayOneShot(Flash_Take);
                    flashlight.SetActive(false);
                }
            }
            // else if(Item.CompareTag("Antidote"))
            // {
            //     text.text = "Press E to use antidote";
            //     if(Input.GetKeyDown("e"))
            //     {
            //         GameObject.Find("Player_Body").GetComponent<Player_Movement>().havesprint = true;
            //         PlayerAudioSource.PlayOneShot(Key_Take);
            //         Destroy(antidote);
            //         text.text = "You have used the antitode your legs are able to move freely";
            //     }
            // }
            else if(Item.CompareTag("Note"))
            {
                text.text = "Press E to search book";
                if(Input.GetKeyDown("e"))
                {
                    GameObject.Find("Note_1").GetComponent<Note_1>().PickNote();
                    PlayerAudioSource.PlayOneShot(Note_Take);
                }
            }
            else if(Item.CompareTag("Note2"))
            {
                text.text = "Press E to search book";
                if(Input.GetKeyDown("e"))
                {
                    GameObject.Find("Note_2").GetComponent<Note_1>().PickNote();
                    PlayerAudioSource.PlayOneShot(Note_Take);
                }
            }
            else if(Item.CompareTag("Note3"))
            {
                text.text = "Press E to search book";
                if(Input.GetKeyDown("e"))
                {
                    GameObject.Find("Note_3").GetComponent<Note_1>().PickNote();
                    GameObject.Find("Scare_Trigger").GetComponent<Event_System>().Note3Taken();
                    PlayerAudioSource.PlayOneShot(Note_Take);
                }
            }
            else if(Item.CompareTag("Note4"))
            {
                text.text = "Press E to search book";
                if(Input.GetKeyDown("e"))
                {
                    GameObject.Find("Note_4").GetComponent<Note_1>().PickNote();
                    PlayerAudioSource.PlayOneShot(Note_Take);
                }
            }
            else if(Item.CompareTag("Note5"))
            {
                text.text = "Press E to search book";
                if(Input.GetKeyDown("e"))
                {
                    GameObject.Find("Note_5").GetComponent<Note_1>().PickNote();
                    GameObject.Find("Scare_Trigger").GetComponent<Event_System>().DeathState();
                    PlayerAudioSource.PlayOneShot(Note_Take);
                }
            }
            else if(Item.CompareTag("Note6"))
            {
                text.text = "Press E to search book";
                if(Input.GetKeyDown("e"))
                {
                    GameObject.Find("Note_6").GetComponent<Note_1>().PickNote();
                    GameObject.Find("Scare_Trigger").GetComponent<Event_System>().Note6Taken();
                    PlayerAudioSource.PlayOneShot(Note_Take);
                }
            }
            else if(Item.CompareTag("Notebook"))
            {
                text.text = "Press E to pick up notebook";
                if(Input.GetKeyDown("e"))
                {
                    GameObject.Find("Player_Body").GetComponent<Journal_System>().HaveNote = true;
                }
            }
            else if(Item.CompareTag("Radio"))
            {
                text.text = "Press E to turn off the radio";
                if(Input.GetKeyDown("e"))
                {
                    text.text = "Radio is off";
                }
            }
            else if(Door1_In == true)
            {
                if(Item.CompareTag("Door"))
                {    
                    text.text = "Press E to open door";
                    if(Input.GetKeyDown("e"))
                    {
                        if(Opening_Door == false)
                        {
                            GameObject.Find("Door_1_In").GetComponent<Door>().DoorIn();
                        }
                        
                    }
                }
            }
            else if(Door1_Out == true)
            {
                if(Item.CompareTag("Door"))
                {    
                    text.text = "Press E to open door";
                    if(Input.GetKeyDown("e"))
                    {
                        if(Opening_Door == false)
                        {
                            GameObject.Find("Door_1_Out").GetComponent<Door>().DoorOut();
                        }
                    }
                }
            }
            else if(Door2_In == true)
            {
                if(Item.CompareTag("Door"))
                {    
                    text.text = "Press E to open door";
                    if(Input.GetKeyDown("e"))
                    {
                        if(Opening_Door == false)
                        {
                            GameObject.Find("Door_2_In").GetComponent<Door>().DoorIn();
                        }
                    }
                }
            }
            else if(Door2_Out == true)
            {
                if(Item.CompareTag("Door"))
                {    
                    text.text = "Press E to open door";
                    if(Input.GetKeyDown("e"))
                    {
                        if(Opening_Door == false)
                        {
                            GameObject.Find("Door_2_Out").GetComponent<Door>().DoorOut();
                        }
                    }
                }
            }
            else if(Door3_In == true)
            {
                if(Item.CompareTag("Door"))
                {    
                    if(GameObject.Find("Player_Body").GetComponent<Journal_System>().note2 == true)
                    {
                        text.text = "Press E to open door";
                        if(Input.GetKeyDown("e"))
                        {
                            if(Opening_Door == false)
                            {
                                GameObject.Find("Door_3_In_Lock").GetComponent<Door>().DoorIn();
                            }
                        }
                    }
                    else
                    {
                        text.text = "Door is lock";
                        if(Input.GetKeyDown("e"))
                        {
                            GameObject.Find("Door_3_In_Lock").GetComponent<Door>().DoorLock();
                        }
                    }
                }
            }
            else if(Door4_In == true)
            {
                if(Item.CompareTag("Door_Lock"))
                {
                    text.text = "Door is lock";
                    if(Input.GetKeyDown("e"))
                    {
                        GameObject.Find("Door_1_In_Lock").GetComponent<Door>().DoorLock();
                    }
                }
            }
            else if(Door_Final_In_Lock == true)
            {
                if(Item.CompareTag("Door_Final"))
                {    
                    if(GameObject.Find("Player_Body").GetComponent<Player_Movement>().PlayerHasKey1 == true)
                    {
                        text.text = "Press E to open door";
                        if(Input.GetKeyDown("e"))
                        {
                            if(Opening_Door == false)
                            {
                                GameObject.Find("Door_Final_In_Lock").GetComponent<Door>().DoorIn();
                            }
                        }
                    }
                    else
                    {
                        text.text = "Door is lock";
                        if(Input.GetKeyDown("e"))
                        {
                            GameObject.Find("Door_Final_Out_Lock").GetComponent<Door>().DoorLock();
                        }
                    }
                }
            }
            else if(Door_Final_Out_Lock == true)
            {
                if(Item.CompareTag("Door_Final"))
                {    
                    text.text = "Door is lock";
                    if(GameObject.Find("Player_Body").GetComponent<Player_Movement>().PlayerHasKey1 == true)
                    {
                        text.text = "Press E to open door";
                        if(Input.GetKeyDown("e"))
                        {
                            if(Opening_Door == false)
                            {
                                GameObject.Find("Door_Final_Out_Lock").GetComponent<Door>().DoorOut();
                            }
                        }
                    }
                    else
                    {
                        text.text = "Door is lock";
                        if(Input.GetKeyDown("e"))
                        {
                            GameObject.Find("Door_Final_Out_Lock").GetComponent<Door>().DoorLock();
                        }
                    }
                }
            }
            else
            {
                text.text = "";
            }
            
        }
        else
        {
            text.text = "";
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            looknote = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        if(looknote == true)
        {
            Blur.SetActive(true);
        }
        else
        {
            Blur.SetActive(false);
        }

    }

}

    
