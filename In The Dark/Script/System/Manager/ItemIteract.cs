﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ItemIteract : MonoBehaviour
{
    // public InventoryObj Bag;

    public Material HighlightMat;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;
        Debug.DrawRay(transform.position, forward, Color.green);

        RaycastHit hit; 
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        

        if (Physics.Raycast(ray, out hit))
        {
            var Object = hit.transform;

            if(Object.CompareTag("Item"))
            {
                Debug.Log("HitItem2");
                Object.GetComponent<Item>().DisplayName();
                if(Input.GetKeyDown("e"))
                {
                    Debug.Log("Int played");
                    // Object.GetComponent<Item>().Interaction();
                    var item = Object.GetComponent<Item>();
                    // if (item)
                    // {
                    //     Bag.AddItem(item.ScriptObject, 1);
                    //     Destroy(item.ItemModel);
                    // }
                }

                var ObjectRenderer = Object.GetComponent<Renderer>();
                if(ObjectRenderer != null )
                {
                    ObjectRenderer.material = HighlightMat; 
                }
                // If its a interact or inventory type
                // run its interact or put it into inventory
            }
        }
    }
}
