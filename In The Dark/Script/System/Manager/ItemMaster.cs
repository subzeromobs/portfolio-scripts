﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMaster : MonoBehaviour
{
    public Bed Bed;

    public Food Food;
    
    public Water Water;

    public Medicine Medicine;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void IncSleep()
    {
        Bed.Sleeping();
    }

    public void DecHunger()
    {
        Food.Eat();
    }

    public void DecThirst()
    {
        Water.Drink();
    }

    public void IncPHp()
    {
        Medicine.Heal();
    }
}
