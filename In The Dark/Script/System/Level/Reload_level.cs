﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reload_level : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if( Input.GetKey("return"))
        {
            SceneManager.LoadScene("Level_1", LoadSceneMode.Single);
        }

        if (Input.GetKey("escape"))
        {
            Application.Quit();
            Debug.Log("game has been Close");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if( Input.GetKey("return"))
        {
            SceneManager.LoadScene("Level_1", LoadSceneMode.Single);
        }

        if (Input.GetKey("escape"))
        {
            Application.Quit();
            Debug.Log("game has been Close");
        }
        
    }
}
