﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Start_Game : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Canvas;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadSceneOnClick()
    {
        SceneManager.LoadScene("Level_0", LoadSceneMode.Single);
        Destroy(Canvas);
    }
}
