﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;  
using UnityEngine.UI;

public class ExitGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
            Debug.Log("game has been Close");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
            Debug.Log("game has been Close");
        }
    }

    public void QuitOnClick()
    {
        Application.Quit();
        Debug.Log("game has been Close");
    }

}
