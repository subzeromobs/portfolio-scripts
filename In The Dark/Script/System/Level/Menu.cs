﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public GameObject Menu_UI;
    public GameObject Play_UI;
    public GameObject PlayerCam;
    public GameObject MenuCam;
    public GameObject Candle_Menu;
    public GameObject Candle_Play;
    public Animator transition;
    public AudioSource Radio;
    public AudioClip Radio_Play;
    private bool Radio_Played = false;
    public float transitionTime = 1.5f;
    // Start is called before the first frame update
    void Start()
    {
        // PlayerCam.SetActive(false);
        Candle_Play.SetActive(false);
        Radio_Played = false;
        GameObject.Find("Player_Body").GetComponent<Journal_System>().InMenu = true;
        Cursor.lockState = CursorLockMode.Confined;
    }

    // Update is called once per frame
    void Update()
    {
        if(Radio_Played == false)
        {
            StartCoroutine(RadioSound());
        }
        
    }

    public void MenuPlay()
    {
        StartCoroutine(GameStartInAnim());
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void EndGame()
    {
        Menu_UI.SetActive(true);
        Play_UI.SetActive(false);
        PlayerCam.SetActive(false);
        MenuCam.SetActive(true);
        Candle_Menu.SetActive(true);
        Candle_Play.SetActive(false);
    }

    IEnumerator GameStartInAnim()
    {
        transition.SetBool("Fade",true);
        yield return new WaitForSeconds(transitionTime);
        transition.SetBool("Fade",false);
        Candle_Menu.SetActive(false);
        Candle_Play.SetActive(true);
        Menu_UI.SetActive(false);
        PlayerCam.SetActive(true);
        GameObject.Find("Player_Body").GetComponent<Journal_System>().InMenu = false;
    }

    IEnumerator RadioSound()
    {
        Radio_Played = true;
        yield return new WaitForSeconds(40f);
        Radio.PlayOneShot(Radio_Play);
    }
}
