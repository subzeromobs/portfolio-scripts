﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New item", menuName ="Item")]
public class Object : ScriptableObject
{
    public string ObjectName;
    public string InvText;
    public int amount;
    public int Weight;
    public int Condition;

    public int Hunger;
    public int Thirst;
    public int Sleep;
    public int Sanity;
    

    public string description; 
    public string pickText;

    public AudioClip InteractSound;

    public bool ItemType;
    public bool InteractType; 
    public bool Bed;
    public bool water;
    public bool Food;
    public bool Battery;
    public bool Flashlight;
    public bool Note;
}
