using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName ="Enemy")]
public class EnemyType : DefaultObj
{
    public int Health;
    public bool IsDead;

    public void Awake()
    {
        type = ObjType.Enemy;
    }
}
