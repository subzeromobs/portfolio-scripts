using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Event", menuName ="Event")]
public class Event_Type : ScriptableObject
{
    public int EventNum;
    public bool IsTrigger;
    public bool AnimScare;
    public bool SoundScare;
    public bool PlayerFreze;
    public bool ItemActive;
    public bool KeyTake;
}
