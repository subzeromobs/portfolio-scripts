using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Drink", menuName =" Drink")]
public class WaterObject : DefaultObj
{
    public int Thirst;

    public void Awake()
    {
        type = ObjType.Drink;
    }
}
