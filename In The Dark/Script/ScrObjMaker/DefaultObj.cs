using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ObjType
{
    Food,
    Drink,
    Weapon,
    Clip,
    Enemy,
    Medicine,
    Default
}

public abstract class DefaultObj : ScriptableObject
{
    public string ObjectName;
    public GameObject ItemPrefeb;
    public ObjType type;
    public bool ItemType;
    public bool InteractType;
    public Sprite ItemIcon; 
    public int amount;
    // public int Weight;
    public int Condition;
    [TextArea (15,20)]
    public string Description;

    public virtual void Use()
    {
        Debug.Log("Using" + ObjectName);
    }
}


