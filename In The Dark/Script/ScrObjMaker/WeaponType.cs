using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName ="Weapon")]
public class WeaponType : DefaultObj
{
    
    public string ObjectName;
    public int amount;

    public int AmmoPerMag;
    public int Rounds;
    public int Condition;
    public int OverheatPershot;
    public int Damage;

    public AudioClip ShootingSound;
    public AudioClip ReloadSound;
    public AudioClip JamSound;

    public bool Magazine;
    public bool Shell;
    public bool Melee;
    public bool FullAuto;
    public bool Sniper;
    public bool Semi;
    
    public void Awake()
    {
        type = ObjType.Weapon;
    }

    public void OnMouseEnter()
    {
        Debug.Log("Mouse is enter");
    }

    public void Equip()
    {
        
    }
}
