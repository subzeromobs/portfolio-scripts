using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Medicine", menuName ="Medicine")]
public class HealObject : DefaultObj
{
    public int Heal;

    public void Awake()
    {
        type = ObjType.Medicine;
    }
}
