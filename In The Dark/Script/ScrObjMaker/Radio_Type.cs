﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Radio", menuName ="Radio")]
public class Radio_Type : ScriptableObject
{
    public int radioNum;
}
