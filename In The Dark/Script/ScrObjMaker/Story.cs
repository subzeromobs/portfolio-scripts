using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Note", menuName ="Note")]
public class Story : ScriptableObject
{
    public string StoryName;
    public int Series;
    [TextArea(15,20)]
    public string NoteStory; 
    public string pickText;
}
