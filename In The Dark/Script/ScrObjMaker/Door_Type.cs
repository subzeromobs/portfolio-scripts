using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Door", menuName ="Door")]
public class Door_Type : ScriptableObject
{
    public bool Door_In;
    public int DoorNum;
    public bool IsLock;
}
