using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Magazine", menuName ="Magazine")]
public class MagazineType : DefaultObj
{
    public string ObjectName;
    public int Amount;
    public int AmmoInMag;
    public GameObject Model;

    public bool M1911;
    public bool Shotgun;
    
    public void Awake()
    {
        type = ObjType.Clip;
    }
}
