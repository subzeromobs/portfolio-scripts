﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Light_Delay : MonoBehaviour
{
    private Vector3 vectOffset;
    public GameObject goFollow;
    public float speed = 10f;
    // Start is called before the first frame update
    void Start()
    {
        goFollow = Camera.main.gameObject;
        vectOffset = transform.position - goFollow.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = goFollow.transform.position + vectOffset;
        transform.rotation = Quaternion.Slerp(transform.rotation, goFollow.transform.rotation, speed * Time.deltaTime);
    }
}
