﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Stat : MonoBehaviour
{
    // Food
    public float Hunger = 100f;

    public bool Hungry = false;

    public bool Starving = false;

    // Drink
    public float Thirst = 100f;

    public bool Thirsty = false;

    public bool dehydrated = false;

    // Sleep
    public float Sleep = 100;

    public bool Sleepy = false;
    
    public bool exhausted = false;

    // Health
    public float PHp = 100;

    public bool BeenHit = false;

    // Sanity
    public float Sanity = 100;

    public bool Insanity = false;

    // Inventory Slot
    public float EquipSlot = 2;
    
    void Start()
    {
        
    }

    void Update()
    {
        Hunger -= 0.01f * Time.deltaTime;
        Thirst -= 0.01f * Time.deltaTime;

        if(Hunger >= 100)
        {
            Hunger = 100;
        }

        if(Thirst >= 100)
        {
            Thirst = 100;
        }

        if(Sleep >= 100)
        {
            Sleep = 100;
        }

        if(Sanity >= 100)
        {
            Sanity = 100;
        }
    }
}
