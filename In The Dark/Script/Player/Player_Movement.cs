using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Movement : MonoBehaviour
{
    // Start is called before the first frame update

    public CharacterController controller;

    public float speed = 1f;
    private float RegularSpeed = 1f;
    private float SprintingSpeed = 3f;
    public float gravity = -9.81f;
    // public float jumpHeight =3f;
    public bool IsStanding = true;
    public bool sprinting = false;
    public bool exhausted = false;
    public bool Adrenaline = false;
    public float stamina = 100f;
    private float TotalStamina = 100f;
    private float StaminaRegen = 2;
    private float ExStaminaRegen = 4;

    public bool haveflashlight = false;
    

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    public static Vector3 cameraPosition;
    public GameObject Player;
    public GameObject PlayerParent;

    public GameObject Flashlight;
    public bool light = false;
    public AudioClip FlashLightOn;
    public float FlashBattery = 100f;
    public bool DimLight = false;

    public bool PlayerHasKey1 = false;

    public AudioSource audioSource;
    public AudioClip Walk;
    public float stepRate = 0.8f;
	public float stepCoolDown;
	public AudioClip footStep;

    Vector3 velocity;
    bool isGrounded;

    Animator animator;

    void start()
    {
        animator = GetComponentInChildren<Animator>();

        // Stamina Regen
    }

    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
        
        // if(Input.GetButtonDown("Jump") && isGrounded)
        // {
        //     velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        // }

        stepCoolDown -= Time.deltaTime;
		if ((Input.GetAxis("Horizontal") != 0f || Input.GetAxis("Vertical") != 0f) && stepCoolDown < 0f){
            StartCoroutine(FootStep_Wood());
			stepCoolDown = stepRate;
		}

        if((Input.GetButton("Horizontal") || Input.GetButton("Vertical")))
        {
            IsStanding = false;
        }
        
        // Sprinting
        if((Input.GetButton("Horizontal") || Input.GetButton("Vertical")) && Input.GetButton("Sprint"))
        {
            if(exhausted == false)
            {
                sprinting = true;
            }
        }
        else
        {
            sprinting = false;
        }

        // prevent sprinting when exhausted
        if(sprinting == true && stamina <= 0)
        {
            sprinting = false;
            exhausted = true;
        }

        // When Sprinting and when not
        if(sprinting == true)
        {
            speed = SprintingSpeed;
            stamina -= 10f * Time.deltaTime;
        }
        else
        {
            speed = RegularSpeed;
            if(stamina <= 100)
            {
                stamina += StaminaRegen * Time.deltaTime;
            }
            
            if(stamina >= 100)
            {
                stamina = TotalStamina;
            }
        }
        // exhausted
        if(exhausted == true)
        {
            speed -= SprintingSpeed;
            sprinting = false;
            if(stamina <= 30)
            {   
                stamina += ExStaminaRegen * Time.deltaTime;
            }
        }

        if(stamina >= 30)
        {
            exhausted = false;
        }

        // addrenaline 
        if(Adrenaline == true)
        {
            stamina = 150;
        }
        else
        {
            stamina = TotalStamina;
        }

        if(FlashBattery <= 0)
        {
            FlashBattery = 0;
            Flashlight.SetActive(false);
            light = false;
        }

        // if(DimLight == true)
        // {
        //     animator.SetBool("Dim_Flashlight" , true);
        // }
        // else
        // {
        //     animator.SetBool("Dim_Flashlight" , false);
        // }

        if(FlashBattery <= 30)
        {
            DimLight = true;
            // darkness system
            float isn = Random.Range(0,1);
            if(isn <= 1)
            {
                Debug.Log("scare happen");
            }
        }
        else
        {
            DimLight = false;
        }

        if(light == true)
        {
            FlashBattery -=1 * Time.deltaTime;
            
        }
        else
        {
            FlashBattery = FlashBattery;
        }

        if(Input.GetKeyDown("f"))
        {
            if(haveflashlight == false)
            {
                Flashlight.SetActive(false);
            }
            else
            {
                if(light == false && FlashBattery >= 0)
                {
                    Flashlight.SetActive(true);
                    audioSource.PlayOneShot(FlashLightOn);
                    light = true;
                }
                else
                {
                    Flashlight.SetActive(false);
                    audioSource.PlayOneShot(FlashLightOn);
                    light = false;
                }
            }
        }
    }

    public void PlayerMovePause()
    {
        speed = 0f;
    }

    public void PlayerMoveUnpause()
    {
        speed = 1f;
    }

    IEnumerator FootStep_Wood()
    {
        audioSource.PlayOneShot(footStep, 0.1f);
        yield return new WaitForSeconds(0.3f);
        audioSource.PlayOneShot(Walk, 0.1f);
    }
    
}
