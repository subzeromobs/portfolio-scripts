using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public EnemyType EnemyStat;

    public int Hp;

    // Start is called before the first frame update
    void Start()
    {
        Hp = EnemyStat.Health;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
