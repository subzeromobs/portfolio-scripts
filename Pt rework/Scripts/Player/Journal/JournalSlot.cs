using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JournalSlot : MonoBehaviour
{
    public Transform JournalParent;
    public string missingNoteName;
    public Sprite readingNote;
    public GameObject readNoteUi;
    DefaultNote note;
    // Start is called before the first frame update
    void Start()
    {
        missingNoteName = "Missing Note";
        readNoteUi.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NoteAdded(DefaultNote newNote)
    {
        note = newNote;
        missingNoteName = newNote.itemName;
        readingNote = note.itmSprite;
    }

    public void EmptySlot()
    {
        note = null;
        missingNoteName = "Missing Note";
    }

    public void ReadNote()
    {
        if(note != null)
        {
            readingNote = note.itmSprite;
            readNoteUi.SetActive(true);
        }
    }

    public void NotReading()
    {
        readNoteUi.SetActive(false);
    }
}
