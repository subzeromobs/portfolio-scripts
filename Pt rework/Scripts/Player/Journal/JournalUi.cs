using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JournalUi : MonoBehaviour
{
    public Transform journalSlotParent;
    public GameObject journalUi;
    PlayerJournal journal;
    JournalSlot[] slots;
    // Start is called before the first frame update
    void Start()
    {
        slots = journalSlotParent.GetComponentsInChildren<JournalSlot>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void UpdateUi()
    {
        for(int i = 0;i < slots.Length;i++)
        {
            if(i < journal.journalList.Count)
            {
                slots[i].NoteAdded(journal.journalList[i]);
            }
            else
            {
                slots[i].EmptySlot();
            }
        }
    }
}
