using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerInteracting : MonoBehaviour
{
    [SerializeField] InputManager inputManager;
    [SerializeField] TMP_Text hoverText;
    [SerializeField] NoteUi noteUi;
    [SerializeField] PlayerMovement playerMovement;
    Transform cam;
    
    float HelpText = 3;
    // Start is called before the first frame update
    void Start()
    {
        inputManager = InputManager.Instance;
        cam = Camera.main.transform; 
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;

        RaycastHit hit; 
    
        if (Physics.Raycast(cam.position,cam.forward , out hit))
        {
            var Object = hit.transform;

            if(Object.CompareTag("Item"))
            {
                Debug.Log("hit item");
                var hoverItem = Object.GetComponent<GameItem>();
                HelpText -= 1 * Time.deltaTime;
                if(HelpText == 0)
                {
                    hoverText.text = "Press interaction to use" + hoverItem.gameItem.itemName;
                }
                if(inputManager.Interacting())
                {
                    Debug.Log("interacting with" + hoverItem.gameItem.itemName);
                    hoverItem.gameItem.Use();
                    if(hoverItem.itemNote)
                    {
                        Debug.Log("there is a note");
                        // var noteItem = hoverItem.
                        noteUi.noteUiSprite.sprite = hoverItem.gameItem.itmSprite;
                        noteUi.ShowNote();
                    }
                    if(hoverItem.itemDoor)
                    {
                        Debug.Log("there is a Door");
                        hoverItem.PlayDoorAnim();
                        playerMovement.openingDoor = true;
                    }
                    if(hoverItem.itemKeyItem)
                    {
                        Debug.Log("there is a KeyItem");
                    }
                }
            }
        }
        else
        {
            HelpText = 3;
            hoverText.text = "";
        }

        if(inputManager.CloseUi())
        {
            noteUi.CloseNote();
        }
    }
}
