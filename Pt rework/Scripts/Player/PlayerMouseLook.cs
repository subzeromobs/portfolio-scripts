using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMouseLook : MonoBehaviour
{
    [SerializeField] InputManager inputManager;
    public float mouseSensitivity = 100f;
    public Transform playerBody;
    float xRotation = 0f;
    // Start is called before the first frame update
    void Start()
    {
        inputManager = InputManager.Instance;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 deltaInput = inputManager.GetMouseDelta();

        float mouseX = deltaInput.x * mouseSensitivity * Time.deltaTime;
        float mouseY = deltaInput.y * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);
    }

    public void PlayerMousePause()
    {
        mouseSensitivity = 0f;
    }

    public void PlayerMouseUnpause()
    {
        mouseSensitivity = 100f;
    }
    
    public void PlayerShowCursor()
    {
        Cursor.lockState = CursorLockMode.Confined;
    }

    public void PlayerHideCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }
}
