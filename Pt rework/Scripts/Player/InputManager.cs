using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    static InputManager _instance;
    public static InputManager Instance
    {
        get
        {
            return _instance;
        }
    }
    PlayerInput playerInput;
    void Awake()
    {
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        playerInput = new PlayerInput();
        DontDestroyOnLoad (gameObject);
    }

    void OnEnable()
    {
        playerInput.Enable();
    }

    void OnDisable()
    {
        playerInput.Disable();
    }

    public Vector2 GetPlayerMovement()
    {
        return playerInput.Player.Move.ReadValue<Vector2>();
    }

    public Vector2 GetMouseDelta()
    {
        return playerInput.Player.Look.ReadValue<Vector2>();
    }

    public bool Interacting()
    {
        return playerInput.Player.Interaction.triggered;
    }

    public bool CloseUi()
    {
        return playerInput.Player.CloseUi.triggered;
    }
}
