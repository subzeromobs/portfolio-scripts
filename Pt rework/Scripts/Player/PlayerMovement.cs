using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] CharacterController controller;
    [SerializeField] InputManager inputManager;
    public float speed = 1f;
    float baseSpeed;
    public float gravity = -9.81f;
    [SerializeField] Transform groundCheck;
    [SerializeField] float groundDistance = 0.4f;
    [SerializeField] LayerMask groundMask;
    [SerializeField] AudioSource audioSource;
    [SerializeField] float stepRate = 0.8f;
	float stepCoolDown;
	[SerializeField] AudioClip footStep_1;
    [SerializeField] AudioClip footStep_2;
    Vector3 velocity;
    bool isGrounded;
    public bool openingDoor = false;
    float movementX;
    float movementY;
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        controller = this.gameObject.GetComponent<CharacterController>();
        inputManager = InputManager.Instance;
        openingDoor = false;
        baseSpeed = speed;
    }

    // Update is called once per frame
    void Update()
    {
        // basic movement
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        Vector2 movement = inputManager.GetPlayerMovement();
        float x = movementX;
        float y = movementY;

        if(openingDoor)
        {
            StartCoroutine(PlayerOpeningDoor());
        }
        else
        {
            movementX = movement.x;
            movementY = movement.y;
        }

        Vector3 move = transform.right * x + transform.forward * y;

        controller.Move(move * speed * Time.deltaTime);

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        // footstep sound
        stepCoolDown -= Time.deltaTime;
		if ((movement.x != 0f || movement.y != 0f) && stepCoolDown < 0f)
        {
            StartCoroutine(FootStep());
			stepCoolDown = stepRate;
		}
    }

    IEnumerator FootStep()
    {
        audioSource.PlayOneShot(footStep_1, 0.1f);
        yield return new WaitForSeconds(0.3f);
        audioSource.PlayOneShot(footStep_2, 0.1f);
    }

    IEnumerator PlayerOpeningDoor()
    {
        movementX = 0;
        movementY = 1;
        yield return new WaitForSeconds(1f);
        openingDoor = false;
    }

    public void PlayerStopMove()
    {
        speed = 0;
    }
    public void PlayerResumeMove()
    {
        speed = baseSpeed;
    }
}
