using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameItem : MonoBehaviour
{
    public Interactable gameItem;
    public GameObject animatorParent;
    public Animator animation;
    // Start is called before the first frame update
    public bool itemNote = false;
    public bool itemDoor = false;
    public bool itemKeyItem = false;
    void Start()
    {
        if(gameItem.itmType == Interactable.ItmType.Note)
        {
            itemNote = true;
        }

        if(gameItem.itmType == Interactable.ItmType.Door)
        {
            itemDoor = true;
            animation = animatorParent.GetComponent<Animator>();
        }

        if(gameItem.itmType == Interactable.ItmType.KeyItem)
        {
            itemKeyItem = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayDoorAnim()
    {
        animation.SetBool("ThisWork" , true);
    }


}
