using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Note", menuName = "Note")]
public class DefaultNote : Interactable
{
    public float noteId;
    public void Awake()
    {
        itmType = ItmType.Note;
    }
    public override void Use()
    {
        Debug.Log("added note to journal");
    }
}
