using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Door", menuName = "Door")]
public class DefaultDoor1 : Interactable
{
    [SerializeField] RoomManager roomManager;
    public void Awake()
    {
        itmType = ItmType.Door;
        roomManager = GameObject.FindGameObjectWithTag("RoomManager").GetComponent<RoomManager>();
    }
    public override void Use()
    {
        roomManager.timePlayerPassARoom ++;
        // Play open door animation
    }
}
