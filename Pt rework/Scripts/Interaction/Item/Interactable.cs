using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : ScriptableObject
{
    public string itemName;
    public ItmType itmType;
    public Sprite itmSprite;
    public virtual void Use()
    {
        Debug.Log("using this"+ itemName);
    }

    public enum ItmType
    {
        Door,
        KeyItem,
        Note
    }
}
