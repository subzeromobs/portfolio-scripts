using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviour
{
    public int timePlayerPassARoom;
    [SerializeField] GameObject startSpawn;
    [SerializeField] GameObject room_4;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(timePlayerPassARoom == 1)
        {
            startSpawn.SetActive(false);
            room_4.SetActive(true);
        }
    }
}
