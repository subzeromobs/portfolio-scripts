using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomPassCounter : MonoBehaviour
{
    [SerializeField] RoomManager roomManager;
    void Start()
    {
        roomManager = GameObject.FindGameObjectWithTag("RoomManager").GetComponent<RoomManager>();
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player")) 
        {
            roomManager.timePlayerPassARoom ++;
        }
    }
}
