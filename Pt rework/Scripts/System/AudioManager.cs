using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource bgAudioSource;
    public AudioSource effectAudioSource;
    public AudioClip bg_1;
    public AudioClip bg_2;
    public AudioClip doorClose_1;
    public AudioClip doorClose_2;
    public AudioClip noteSound_1;
    public AudioClip noteSound_2;
    public AudioClip radioVoice_1;
    public AudioClip radioVoice_2;
    public AudioClip radioVoice_3;
    public AudioClip radioVoice_4;
    public AudioClip chandelierCreak;
    public AudioClip playerFootstep_1;
    public AudioClip playerFootstep_2;
    public AudioClip playerHeart;
    public AudioClip monsterFootstep_1;
    public AudioClip monsterFootstep_2;
    public AudioClip itemDrop_1;

    // public List<AudioClip> audioList = new List<AudioClip>();

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    // public void PlayAudio(AudioClip audioClip)
    // {
    //     // audioClip ;
    //     effectAudioSource.PlayOneShot(audioClip);
    // }

    public void PlayBg_1()
    {
        bgAudioSource.PlayOneShot(bg_1);
    }
    public void PlayBg_2()
    {
        bgAudioSource.PlayOneShot(bg_2);
    }

    public void PlaydoorClose_1()
    {
        bgAudioSource.PlayOneShot(doorClose_1);
    }

    public void PlaydoorClose_2()
    {
        bgAudioSource.PlayOneShot(doorClose_2);
    }
    
    public void PlaynoteSound_1()
    {
        bgAudioSource.PlayOneShot(noteSound_1);
    }

    public void PlaynoteSound_2()
    {
        bgAudioSource.PlayOneShot(noteSound_2);
    }

    public void PlayradioVoice_1()
    {
        bgAudioSource.PlayOneShot(radioVoice_1);
    }

    public void PlayradioVoice_2()
    {
        bgAudioSource.PlayOneShot(radioVoice_2);
    }

    public void PlayradioVoice_3()
    {
        bgAudioSource.PlayOneShot(radioVoice_3);
    }

    public void PlayradioVoice_4()
    {
        bgAudioSource.PlayOneShot(radioVoice_4);
    }

    public void PlaychandelierCreak()
    {
        bgAudioSource.PlayOneShot(chandelierCreak);
    }

    public void PlayplayerHeart()
    {
        bgAudioSource.PlayOneShot(playerHeart);
    }

    public void PlaymonsterFootstep_1()
    {
        bgAudioSource.PlayOneShot(monsterFootstep_1);
    }

    public void PlaymonsterFootstep_2()
    {
        bgAudioSource.PlayOneShot(monsterFootstep_2);
    }

    public void PlayitemDrop_1()
    {
        bgAudioSource.PlayOneShot(itemDrop_1);
    }
}
