using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RoomGen : MonoBehaviour 
{
    [SerializeField] GameObject RoomPrefeb_1;
    public bool genNextRoom;
    [SerializeField] RoomTrigger roomTrigger;
    [SerializeField] bool roomIsGen = false;
    public int CurrentRoom;
    [SerializeField] UnityEvent LoadingNextRoom;
    void Start()
    {

    }

    // Update is called once per frame 
    void Update()
    {
        if(genNextRoom)
        {
            // increase the room number and instantiate the next room
            if(!roomIsGen)
            {
                LoadingNextRoom.Invoke();
                roomIsGen = true;
            }
        }
    }

    public void NextRoomStart()
    {
        GameObject newRoom = Instantiate(RoomPrefeb_1) as GameObject;
        return;
    }

    public void DestroyRoom()
    {
        Destroy(gameObject);
    }
}
