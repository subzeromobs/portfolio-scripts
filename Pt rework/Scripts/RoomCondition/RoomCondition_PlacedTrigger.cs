using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RoomTrigger_ObjTake", menuName = "RoomCondition/Placed Trigger")]
public class RoomCondition_PlacedTrigger : DefaultRoomCondition
{
    public void Awake()
    {
        triType = TriType.TriggerEnter;
    }
    public virtual void RoomConditionTrue()
    {
       ReadyNextRoom();
    }
}
