using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RoomTrigger_ObjTake", menuName = "RoomCondition/Interaction")]
public class RoomCondition_Interaction : DefaultRoomCondition
{
    public void Awake()
    {
        triType = TriType.InteractionType;
    }
    
    public virtual void RoomConditionTrue()
    {
       if(haveInteracted)
       {
           ReadyNextRoom();
       }
    }
}
