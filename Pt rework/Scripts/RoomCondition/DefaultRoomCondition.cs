using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TriType
{
    TriggerEnter,
    ItemTake,
    InteractionType,
    Default
}
public class DefaultRoomCondition : ScriptableObject
{
    public TriType triType;
    public bool roomIsTrigger;
    public bool haveKeyItem = false;
    public bool haveInteracted = false;
    public void ReadyNextRoom()
    {
        roomIsTrigger = true;
    }

    public virtual void RoomConditionTrue()
    {
       Debug.Log("Next Room Condition is met");
    }
}


