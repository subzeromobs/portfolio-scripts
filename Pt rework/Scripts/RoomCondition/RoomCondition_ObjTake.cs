using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RoomTrigger_ObjTake", menuName = "RoomCondition/Obj Take")]
public class RoomCondition_ObjTake : DefaultRoomCondition
{
    public void Awake()
    {
        triType = TriType.ItemTake;
    }
    
    public virtual void RoomConditionTrue()
    {
       if(haveKeyItem)
       {
           ReadyNextRoom();
       }
    }
}
