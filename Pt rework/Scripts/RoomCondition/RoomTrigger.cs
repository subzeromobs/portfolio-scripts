using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTrigger : MonoBehaviour
{
    [SerializeField] RoomGen roomGen;
    [SerializeField] DefaultRoomCondition roomTrigger; 
    [SerializeField] bool keyItem;
    [SerializeField] bool interacted;
    void Start()
    {
        roomGen = this.gameObject.GetComponent<RoomGen>();
    }
    void Update()
    {
        if(roomTrigger.triType == TriType.ItemTake)
        {
            if(keyItem)
            {
                roomTrigger.haveKeyItem = true;
            }
        }

        if(roomTrigger.triType == TriType.InteractionType)
        {
            if(interacted)
            {
                roomTrigger.haveInteracted = true;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(roomTrigger.triType == TriType.TriggerEnter)
        {
            if(other.gameObject.CompareTag("Player")) 
            {
                roomTrigger.RoomConditionTrue();
            }
        }
    }
}
