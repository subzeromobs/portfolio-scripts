using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiStateMachine : MonoBehaviour
{
    public EnemyAiState[] states;
    public Enemy enemy;
    public AiStateId currentState;
    public void Update()
    {
        GetState(currentState)?.Update(enemy);
    }
    public AiStateMachine(Enemy enemy)
    {
        this.enemy = enemy;
        int numStates = System.Enum.GetNames(typeof(AiStateId)).Length;
        states = new EnemyAiState[numStates];
    }

    public void RegisterState(EnemyAiState state)
    {
        int index = (int)state.GetId();
        states[index] = state;
    }

    public EnemyAiState GetState(AiStateId stateId)
    {
        int index = (int)stateId;
        return states[index];
    }

    public void ChangeState(AiStateId newState)
    {
        GetState(currentState)?.Exit(enemy);
        currentState = newState;
        GetState(currentState)?.Enter(enemy);
    }
}
