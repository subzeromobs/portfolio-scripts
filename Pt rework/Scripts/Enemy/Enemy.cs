using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Transform Player;
    public float MoveSpeed;
    public bool chasingPlayer;
    public bool doneChasing;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Chase()
    {
        transform.LookAt(Player);
        transform.Translate(Vector3.forward * MoveSpeed * Time.deltaTime);
    }
}
