using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteUi : MonoBehaviour
 {
    public GameObject noteUI;
    public SpriteRenderer noteUiSprite;
    [SerializeField] PlayerMouseLook playerMouse;
    [SerializeField] PlayerMovement playerMove;
    // Start is called before the first frame update
    void Start()
    {
        noteUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowNote()
    {
        noteUI.SetActive(true);
        playerMouse.PlayerShowCursor();
        playerMouse.PlayerMousePause();
        playerMove.PlayerStopMove();
    }

    public void CloseNote()
    {
        playerMouse.PlayerMouseUnpause();
        playerMouse.PlayerHideCursor();
        playerMove.PlayerResumeMove();
        noteUI.SetActive(false);
        noteUiSprite.sprite = null;
    }
}
