﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio_Manager : MonoBehaviour
{
    public AudioClip Reload;
    public AudioClip EjectMag;
    public AudioClip Shoot;
    public AudioClip FootStep;
    public AudioClip Jump;
    public AudioClip land;
    public AudioClip Hit;
    public AudioClip Hit_Wall;
    public AudioClip Hit_Player;
    public AudioClip DeadEnemy;
    public AudioClip Background_1;
    public AudioClip Menu_1;
    public AudioClip GameEnd_1;
    public AudioClip Menu_Click_1;
    public AudioClip Menu_Hover_1;
    public AudioSource Player;
    public AudioSource Enemy;
    public AudioSource Background;

    public bool PlayedBg = false;
    public bool PlayedGEM = false;
    public bool PlayedMM = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void  ReloadGun()
    {
        Player.PlayOneShot(Reload);
    }
    
    public void EjectMagGun()
    {
        Player.PlayOneShot(EjectMag);
    }

    public void ShootGun()
    {
        Player.PlayOneShot(Shoot);
    }
    public void Walk()
    {
        Player.PlayOneShot(FootStep);
    }
    public void Jumping()
    {
        Player.PlayOneShot(Jump);
    }
    public void Landing()
    {
        Player.PlayOneShot(land);
    }
    public void Hit_Sound_1()
    {
        Player.PlayOneShot(Hit);
    }
    public void Hit_Wall_1()
    {
        Player.PlayOneShot(Hit_Wall);
    }
    public void Hit_Player_1()
    {
        Player.PlayOneShot(Hit_Player);
    }
    public void Dead()
    {
        Player.PlayOneShot(DeadEnemy);
    }
    public void BackgroundMusic1()
    {
        if(PlayedBg == false)
        {
            Background.Stop();
            Background.PlayOneShot(Background_1);
            PlayedBg = true;
        }
    }
    public void GameEndMusic()
    {
        if(PlayedGEM == false)
        {
            Background.Stop();
            Background.PlayOneShot(GameEnd_1);
            PlayedGEM = true;
        }
    }
    public void MenuMusic()
    {
        if(PlayedMM == false)
        {
            Background.Stop();
            Background.PlayOneShot(Menu_1);
            PlayedMM = true;
        }
    }
    public void Menu_Click()
    {
        Player.PlayOneShot(Menu_Click_1);
    }
    public void Menu_Hover()
    {
        Player.PlayOneShot(Menu_Hover_1);
    }
    
}
