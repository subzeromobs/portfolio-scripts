﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using TMPro;
using UnityEngine.UI;

public class Game_Volume : MonoBehaviour

{
    public AudioMixer mixer;
    public Slider VolumeSlider;
    [SerializeField] 
    private TextMeshProUGUI VolumeText;

    void update()
    {
        // VolumeText.text = VolumeSlider.value.ToString();
    }
    
    public void SetGameVolume(float sliderValue)
    {
        mixer.SetFloat("GameVolume",Mathf.Log10 (sliderValue) * 20);
    }
}
