﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ui_Manager : MonoBehaviour
{
    public GameObject MenuUi;
    public GameObject PauseUi;
    public GameObject OptionUi;
    public GameObject BackMenu;
    public GameObject BackGame;
    public GameObject PlayerUi;
    public GameObject DeadUi;

    public void MenuUiOn()
    {
        MenuUi.SetActive(true);
    }
    public void MenuUiOff()
    {
        MenuUi.SetActive(false);
    }
    public void PauseUiOn()
    {
        PauseUi.SetActive(true);
    }
    public void PauseUiOff()
    {
        PauseUi.SetActive(false);
    }
    public void OptionUiOn()
    {
        OptionUi.SetActive(true);
    }
    public void OptionUiOff()
    {
        OptionUi.SetActive(false);
    }
    public void BackMenuOn()
    {
        BackMenu.SetActive(true);
    }
    public void BackMenuOff()
    {
        BackMenu.SetActive(false);
    }
    public void BackGameOn()
    {
        BackGame.SetActive(true);
    }
    public void BackGameOff()
    {
        BackGame.SetActive(false);
    }
    public void PlayerUiOn()
    {
        PlayerUi.SetActive(true);
    }
    public void PlayerUiOff()
    {
        PlayerUi.SetActive(false);
    }
    
    public void DeadUiOn()
    {
        DeadUi.SetActive(true);
    }
    public void DeadUiOff()
    {
        DeadUi.SetActive(false);
    }
}
