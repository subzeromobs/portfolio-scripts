﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score_System : MonoBehaviour
{
    public Player Player;
    public TextMeshProUGUI ScoreText;
    public TextMeshProUGUI TotalScoreText;
    public float Score = 0;
    public float ScorePerSecond = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TotalScoreText.text = Score.ToString();
        ScoreText.text = Score.ToString();
        Score += ScorePerSecond * Time.deltaTime;

        // if(Player.PlayerHp <= 0)
        // {
        //     ScorePerSecond = 0;
        // }
        // else
        // {
        //     SC
        // }
    }
}
