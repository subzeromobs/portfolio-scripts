﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Crosshair_Color_Change : MonoBehaviour
{
    public Image Crosshair;
    public Slider ColorSliderRed;
    public Slider ColorSliderGreen;
    public Slider ColorSliderBlue;
    public float Red;
    public float Green;
    public float Blue;
    [SerializeField] 
    private TextMeshProUGUI RedText;
    [SerializeField] 
    private TextMeshProUGUI GreenText;
    [SerializeField] 
    private TextMeshProUGUI BlueText;
    void Start()
    {
        
    }

    void Update()
    {
        Color color = Crosshair.material.color;
        color.r = Red;
        color.g = Green;
        color.b = Blue;
        Red = ColorSliderRed.value;
        Blue = ColorSliderBlue.value;
        Green = ColorSliderGreen.value;
        Crosshair.material.color = color;
        Crosshair.material.SetColor("_materialColor", color);
        RedText.text = Red.ToString();
        GreenText.text = Green.ToString();
        BlueText.text = Blue.ToString();
    }
}
