﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Crosshair_Size : MonoBehaviour
{
    public Image CrosshairSetting;
    public Image CrosshairGame;
    public Slider SizeHeight;
    public Slider SizeWidth;
    public float CrossWidth;
    public float CrossHeight;
    [SerializeField] 
    private TextMeshProUGUI SizeText;
    // Update is called once per frame
    void Update()
    {
        CrosshairSetting.rectTransform.sizeDelta = new Vector2(CrossWidth , CrossHeight);

        CrosshairGame.rectTransform.sizeDelta = new Vector2(CrossWidth , CrossHeight);
        
        CrossWidth = SizeWidth.value;

        CrossHeight = SizeHeight.value;

        SizeText.text = CrossWidth.ToString();
    }
}
