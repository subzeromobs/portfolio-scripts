﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Mouse_Sense : MonoBehaviour
{
    public Slider SensitivitySlider;
    public Mouse_Look Mouse_Look;
    [SerializeField] 
    private TextMeshProUGUI SenseText;

    void Update()
    {
        Mouse_Look.MouseSensitivity = SensitivitySlider.value;

        SenseText.text = Mouse_Look.MouseSensitivity.ToString();
    }
}
