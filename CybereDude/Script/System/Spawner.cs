﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject Enemy;
    [SerializeField]
    private Transform EnemySpawn;
    [SerializeField]
    private float StageTimer = 0f;
    public float SpawnDownDur = 2f;
    public float Stage1Timer = 0f;
    public float Stage2Timer = 0f;
    public float Stage3Timer = 0f;
    public float Stage4Timer = 0f;
    public float Stage5Timer = 0f;
    public float SpawnChance = 0f;
    public float Stage1SpawnChance = 0f;
    public float Stage2SpawnChance = 0f;
    public float Stage3SpawnChance = 0f;
    public float Stage4SpawnChance = 0f;
    public float Stage5SpawnChance = 0f;
    private float SpawnCooldown = 0f;
    public bool StartSpawn = false;
    private bool Spawned = false;
    private bool Stage1 = false;
    private bool Stage2 = false;
    private bool Stage3 = false;
    private bool Stage4 = false;
    private bool Stage5 = false;

    void start()
    {
        SpawnCooldown = SpawnDownDur;
    }

    // Update is called once per frame
    void Update()
    {
        SpawnChance = ((Random.value * (Random.value * Random.value)) %1);

        if(Spawned == true)
        {
            SpawnCooldown -= 1 * Time.deltaTime;
        }

        if(SpawnCooldown <= 0)
        {
            Spawned = false;
            SpawnCooldown = SpawnDownDur;
        }

        if(StageTimer >= Stage1Timer)
        {
            Stage1 = true;
            if(Stage1 == true)
            {
                if(SpawnChance > Stage1SpawnChance)
                { 
                    if(Spawned == false)
                    {
                        Instantiate(Enemy , EnemySpawn.position , EnemySpawn.rotation);
                        Spawned = true;
                    }
                }
            }
        }

        if(StageTimer >= Stage2Timer)
        {
            Stage1 = false;
            Stage2 = true;
            if(Stage2 == true)
            {
                if(SpawnChance > Stage2SpawnChance)
                { 
                    if(Spawned == false)
                    {
                        Instantiate(Enemy , EnemySpawn.position , EnemySpawn.rotation);
                        Spawned = true;
                    }
                }
            }
        }

        if(StageTimer >= Stage3Timer)
        {
            Stage2 = false;
            Stage3 = true;
            if(Stage3 == true)
            {
                if(SpawnChance > Stage3SpawnChance)
                { 
                    if(Spawned == false)
                    {
                        Instantiate(Enemy , EnemySpawn.position , EnemySpawn.rotation);
                        Spawned = true;
                    }
                }
            }
        }

        if(StageTimer >= Stage4Timer)
        {
            Stage3 = false;
            Stage4 = true;
            if(Stage4 == true)
            {
                if(SpawnChance > Stage4SpawnChance)
                { 
                    if(Spawned == false)
                    {
                        Instantiate(Enemy , EnemySpawn.position , EnemySpawn.rotation);
                        Spawned = true;
                    }
                }
            }
        }

        if(StageTimer >= Stage5Timer)
        {
            Stage4 = false;
            Stage5 = true;
            if(Stage5 == true)
            {
                if(SpawnChance > Stage5SpawnChance)
                { 
                    if(Spawned == false)
                    {
                        Instantiate(Enemy , EnemySpawn.position , EnemySpawn.rotation);
                        Spawned = true;
                    }
                }
            }
        }
    }

    public void StartSpawning()
    {
        StageTimer += 1 * Time.deltaTime;
    }

    public void StopSpawning()
    {
        StageTimer = 0;
    }
}
