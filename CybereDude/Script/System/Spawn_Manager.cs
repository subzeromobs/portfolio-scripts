﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_Manager : MonoBehaviour
{
    public Spawner Spawner1;
    public Spawner Spawner2;
    public Spawner Spawner3;
    public Spawner Spawner4;
    public Spawner Spawner5;
    public Spawner Spawner6;
    public Spawner Spawner7;
    public Spawner Spawner8;
    public Spawner Spawner9;
    public Spawner Spawner10;
    public Spawner Spawner11;
    public Spawner Spawner12;
    public Spawner Spawner13;
    public Spawner Spawner14;
    public Spawner Spawner15;
    public Spawner Spawner16;
    public Spawner Spawner17;
    public Spawner Spawner18;
    public Spawner Spawner19;
    public Spawner Spawner20;

    public void Starting()
    {
        Spawner1.StartSpawning();
        Spawner2.StartSpawning();
        Spawner3.StartSpawning();
        Spawner4.StartSpawning();
        Spawner5.StartSpawning();
        Spawner6.StartSpawning();
        Spawner7.StartSpawning();
        Spawner8.StartSpawning();
        Spawner9.StartSpawning();
        Spawner10.StartSpawning();
        Spawner11.StartSpawning();
        Spawner12.StartSpawning();
        Spawner13.StartSpawning();
        Spawner14.StartSpawning();
        Spawner15.StartSpawning();
        Spawner16.StartSpawning();
        Spawner17.StartSpawning();
        Spawner18.StartSpawning();
        Spawner19.StartSpawning();
        Spawner20.StartSpawning();
    }

    public void Stoping()
    {
        Spawner1.StopSpawning();
        Spawner2.StopSpawning();
        Spawner3.StopSpawning();
        Spawner4.StopSpawning();
        Spawner5.StopSpawning();
        Spawner6.StopSpawning();
        Spawner7.StopSpawning();
        Spawner8.StopSpawning();
        Spawner9.StopSpawning();
        Spawner10.StopSpawning();
        Spawner11.StopSpawning();
        Spawner12.StopSpawning();
        Spawner13.StopSpawning();
        Spawner14.StopSpawning();
        Spawner15.StopSpawning();
        Spawner16.StopSpawning();
        Spawner17.StopSpawning();
        Spawner18.StopSpawning();
        Spawner19.StopSpawning();
        Spawner20.StopSpawning();
    }
    
}
