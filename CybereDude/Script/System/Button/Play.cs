﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Play : MonoBehaviour
{   
    public State_System State_System;
    public void PlayGame()
    {
        GameObject.Find("AudioManager").GetComponent<Audio_Manager>().Menu_Click();
        GameObject.Find("StateManager").GetComponent<State_System>().PlayState = true;
    }
    public void HoverButton()
    {
        GameObject.Find("AudioManager").GetComponent<Audio_Manager>().Menu_Hover();
    }
    
    public void ResumeGame()
    {
        State_System.PauseState = false;
    }
    public void RetryGame()
    {
        State_System.RetryState = true;
    }
}
