﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting : MonoBehaviour
{
    public Ui_Manager UiManager;
    public State_System State_System;
    
    public void BackMenu()
    {
        UiManager.OptionUiOff();
        UiManager.MenuUiOn();
        GameObject.Find("AudioManager").GetComponent<Audio_Manager>().Menu_Click();
    }
    public void BackGame()
    {
        UiManager.OptionUiOff();
        UiManager.PauseUiOn();
        State_System.PlaySettingState = false;
        GameObject.Find("AudioManager").GetComponent<Audio_Manager>().Menu_Click();
    }
    public void SettingMenu()
    {
        if(State_System.PlayState == true)
        {
            State_System.PlaySettingState = true;
        }
        UiManager.OptionUiOn();
        UiManager.MenuUiOff();
        UiManager.PlayerUiOff();
        UiManager.PauseUiOff();
        GameObject.Find("AudioManager").GetComponent<Audio_Manager>().Menu_Click();
    }
    public void HoverButton()
    {
        GameObject.Find("AudioManager").GetComponent<Audio_Manager>().Menu_Hover();
    }
}
