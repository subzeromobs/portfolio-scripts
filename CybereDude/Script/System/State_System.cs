﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State_System : MonoBehaviour
{
    public Ui_Manager UiManager;
    public Audio_Manager AudioManager;
    // public Spawner Spawner;
    public Score_System ScoreSystem;
    public Player Player;

    [SerializeField]
    private Transform PlayerSpawn;
    
    [SerializeField]
    private Transform PlayerModel;
    public bool PauseState = false;
    public bool PlayState = false;
    public bool PlaySettingState = false;
    public bool MenuState = false;
    public bool DeadState = false;
    public bool RetryState = false;
    public GameObject MenuCam;
    public GameObject PlayerCam;
    void Start()
    {
        MenuState = true;
        UiManager.DeadUiOff();
    }

    // Update is called once per frame
    void Update()
    {
        if(PauseState == true)
        {
            Time.timeScale = 0;
            if(PlaySettingState == false)
            {
                UiManager.PauseUiOn();
            }
            GameObject.Find("PlayerCamera").GetComponent<Mouse_Look>().MouseUnlocked();
            // Puase Background music
        }
        else
        {
            Time.timeScale = 1;
            UiManager.PauseUiOff();
            GameObject.Find("PlayerCamera").GetComponent<Mouse_Look>().MouseLocked();
        }

        if(PlayState == true)
        {
            GameObject.Find("Spawner1").GetComponent<Spawn_Manager>().Starting();
            GameObject.Find("Spawner2").GetComponent<Spawn_Manager>().Starting();
            GameObject.Find("Spawner3").GetComponent<Spawn_Manager>().Starting();
            MenuState = false;
            MenuCam.SetActive(false);
            PlayerCam.SetActive(true);
            UiManager.MenuUiOff();
            UiManager.BackGameOn();
            UiManager.BackMenuOff();
            AudioManager.BackgroundMusic1();
            if(PlaySettingState == false)
            {
                UiManager.PlayerUiOn();
            }
        }

        if(MenuState == true)
        {
            GameObject.Find("PlayerCamera").GetComponent<Mouse_Look>().MouseUnlocked();
            MenuCam.SetActive(true);
            UiManager.BackMenuOn();
            UiManager.BackGameOff();
            AudioManager.MenuMusic();
        }

        if(DeadState == true)
        {
            UiManager.DeadUiOn();
            UiManager.PauseUiOff();
            UiManager.PlayerUiOff();
            AudioManager.GameEndMusic();
            MenuCam.SetActive(true);
            Time.timeScale = 0;
            Destroy(GameObject.FindWithTag("Enemy"));
            GameObject.Find("PlayerCamera").GetComponent<Mouse_Look>().MouseUnlocked();
            DeadState = false;
        }

        if(RetryState == true)
        {
            MenuCam.SetActive(false);
            PlayerCam.SetActive(true);
            UiManager.DeadUiOff();
            UiManager.MenuUiOff();
            UiManager.PlayerUiOn();
            Player.PlayerHp = 100;
            ScoreSystem.Score = 0;
            PlayerModel.position = PlayerSpawn.position;
            PauseState = false;
            GameObject.Find("Spawner1").GetComponent<Spawn_Manager>().Stoping();
            GameObject.Find("Spawner2").GetComponent<Spawn_Manager>().Stoping();
            GameObject.Find("Spawner3").GetComponent<Spawn_Manager>().Stoping();
            AudioManager.PlayedBg = false;
            AudioManager.PlayedMM = false;
            AudioManager.PlayedGEM = false;
            RetryState = false;
        }
    }
}
