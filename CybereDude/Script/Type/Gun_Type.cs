﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Gun", menuName ="Gun")]
public class Gun_Type : ScriptableObject
{
    // make a scripatable object call dmg block that dmg player if they are hit or when thy hit important stuff
    public int Damage;
    public bool Pistol;
    public bool Smg;
    public bool Riffle;
}
