﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New DmgBlock", menuName ="DmgBlock")]
public class DamageBlock : ScriptableObject
{
    // make a scripatable object call dmg block that dmg player if they are hit or when thy hit important stuff
    public int Damage;
    public int EnemyHP;
}
