﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Bullet", menuName ="Bullet")]
public class Bullet_Type : ScriptableObject
{
    public bool Projectile;
    public bool HitScan;
}
