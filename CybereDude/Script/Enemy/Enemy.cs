﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public DamageBlock DamageBlock;
    public float MoveSpeed = 1f;
    public bool IsHit = false;
    private bool IsDead = false;

    public int TakenDamage;
    public int ObjHp;

    // Start is called before the first frame update
    void Start()
    {
        ObjHp = DamageBlock.EnemyHP;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 move = transform.forward;

        transform.position += move * MoveSpeed * Time.deltaTime;

        MoveSpeed += 1 * Time.deltaTime;

        if(ObjHp <= 0)
        {
            IsDead = true;
        }

        if(IsDead == true)
        {
            Destroy(gameObject);
            GameObject.Find("AudioManager").GetComponent<Audio_Manager>().Dead();
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.tag == "Player")
        {
            GameObject.Find("Player").GetComponent<Player>().PlayerHp -= DamageBlock.Damage;
            GameObject.Find("AudioManager").GetComponent<Audio_Manager>().Hit_Player_1();
            IsDead = true;
        }

        if(collider.gameObject.tag == "Wall")
        {
            IsDead = true;
        }

        if(collider.gameObject.tag == "Bullet")
        {
            ObjHp -= TakenDamage;
        }
    }
}
