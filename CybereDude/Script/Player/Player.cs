﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    public float PlayerHp = 100;
    public State_System State_System;
    public TextMeshProUGUI HpText;

    void Start()
    {
        
    }

    void Update()
    {
        if(Input.GetKeyDown("p") && State_System.PlayState == true)
        {
            if(State_System.PauseState == true)
            {   
                State_System.PauseState = false;
            }
            else
            {
                State_System.PauseState = true;
            }
        }

        if(PlayerHp <= 0)
        {
            State_System.DeadState = true;
            PlayerHp = 0;
        }

        HpText.text = PlayerHp.ToString();
    }
}
