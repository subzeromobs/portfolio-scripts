﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mouse_Look : MonoBehaviour
{
    // Start is called before the first frame update
    
    public float MouseSensitivity;

    public Transform playerBody;

    float xRotation = 0f;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * MouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * MouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);
    }

    public void MouseLocked()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void MouseUnlocked()
    {
        Cursor.lockState = CursorLockMode.Confined;
    }
}

