﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public Transform ShootDir;
    [SerializeField] 
    private GameObject Bullet;
    void Update()
    {
        // Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // RaycastHit hit; 
        // if (Physics.Raycast(ray, out hit, 20))
        // { 
        //     var Item = hit.transform;
        //     if(Item.CompareTag("Enemy"))
        //     {
        //         if(Input.GetButtonDown("Fire1"))
        //         {
        //            hit.collider.GetComponent<Enemy>().DamageBlock.EnemyHP -= 10;
        //         }
        //     }
        // }

        if(Input.GetButtonDown("Fire1"))
        {
            Instantiate(Bullet , ShootDir.position , ShootDir.rotation);
            GameObject.Find("AudioManager").GetComponent<Audio_Manager>().ShootGun();
        }
    }
}
