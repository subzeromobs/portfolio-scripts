﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Bullet_Type Bullet_Type;
    public Gun_Type Gun_Type;
    public int GunDamage;
    public float BulletSpeed = 1f;
    public float TravelTime = 1f;
    private bool Hit = false;
    void Start()
    {
        GunDamage = Gun_Type.Damage;

        if(Bullet_Type.Projectile == true)
        {
            BulletSpeed = 50f;
        }

        if(Bullet_Type.HitScan == true)
        {
            BulletSpeed = 100f;
        }
    }
    void Update()
    {
        Vector3 move = transform.forward;

        transform.position += move * BulletSpeed * Time.deltaTime;

        BulletSpeed -= TravelTime * Time.deltaTime;

        if(BulletSpeed <= 0)
        {
            Destroy(gameObject);
        }

        if(Hit == true)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.tag == "Enemy")
        {
            collider.GetComponent<Enemy>().TakenDamage = GunDamage;
            collider.GetComponent<Enemy>().IsHit = true;
            Hit = true;
        }

        if(collider.gameObject.tag == "Wall")
        {
           Hit = true;
        }
    }
}