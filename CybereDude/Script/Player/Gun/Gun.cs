﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public float Ammo = 10;
    public bool MagEjected = false;
    public bool Reloded = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //press e to eject magizine and manually put it in with r 
        // has to have a meter where  reload at the right time  gain speed buff
        if(MagEjected == true)
        {
            Ammo = 0;
        }

        if(Reloded == true)
        {
            Ammo = 10;
        }


        if(Input.GetKeyDown("e"))
        {
            MagEjected = true;
            GameObject.Find("AudioManager").GetComponent<Audio_Manager>().EjectMagGun();
        }

        if(Input.GetKeyDown("r"))
        {
            Reloded = true;
            GameObject.Find("AudioManager").GetComponent<Audio_Manager>().ReloadGun();
        }
    }
}
